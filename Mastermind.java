public class Mastermind {

  // 1) Combinaison sècrete

  static class Pion {
    // type agrégé d'un pion, caractérisé par sa couleur, modélisée ici par un carcatère majuscule (ex : 'J'-->jaune)
    // on y indique également par un booléen si ce pion est bien placé ou ou s'il comporte un doublon (une couleur identique) au sein d'une même combinaison, ce qui nous servira dans le calcul de la réponse pour la partie 2)
    // par défaut, ces deux booléens sont faux
    char couleur;
    boolean bienPlace=false;
    boolean doublon=false;
  }

  static class Combinaison {
    // type agrégé d'une combinaison, composée de 4 pions
    Pion p1=new Pion();
    Pion p2=new Pion();
    Pion p3=new Pion();
    Pion p4=new Pion();
  }

  // entrée : entier bi (borne inférieur de l'intervalle)
  // entrée : entier bf (borne supérieure de l'intervalle)
  // bi < bf
  static int hasard(int bi,int bf){
    // est retourné : un nombre entier tiré au sort dans [bi,bf]
    return (int)(Math.random()*(bf+1-bi)+bi);
  }

  // entrée : Combinaison combi (la combinaison à afficher)
  static String afficherCombi(Combinaison combi){
    // est retourné : une chaine de caractère contenant l'affichage de la combinaison sous la forme [CCCC] (chaine que l'on appelera par la suite dans un Ecran.afficher() )
    return '['+""+combi.p1.couleur+""+combi.p2.couleur+""+combi.p3.couleur+""+combi.p4.couleur+']';
  }

  // entrée : entier sort (nombre entier tiré au hasard)
  // sort dans [0,7]
  static char attributionCouleur(int sort){
    // est retourné : le caractère couleur correspondant à l'entier tiré au hasard
    char couleur=' '; // initialisation du caractère
    switch(sort){
      case 0: {couleur='R';} break;
      case 1: {couleur='B';} break;
      case 2: {couleur='J';} break;
      case 3: {couleur='V';} break;
      case 4: {couleur='O';} break;
      case 5: {couleur='G';} break;
      case 6: {couleur='I';} break;
      case 7: {couleur='F';} break;
    }
    return couleur;
  }

  // entrée : Pion p1 et Pion p2 (deux pions dont nous voulons comparer la valeur)
  static boolean couleurEgale(Pion p1, Pion p2){
    // est retourné : vrai si les deux pions sont de couleur identique, faux sinon
    return p1.couleur==p2.couleur;
  }

  static Combinaison combiHasard(){
    // est retourné : une combinaison de 4 couleurs tirées au hasard
    // les boucles while servent ici à éviter les doublons de couleur
    Combinaison combi=new Combinaison();

    combi.p1.couleur=attributionCouleur(hasard(0,7));

    combi.p2.couleur=attributionCouleur(hasard(0,7));
    while (couleurEgale(combi.p2,combi.p1)){
      combi.p2.couleur=attributionCouleur(hasard(0,7));
    }

    combi.p3.couleur=attributionCouleur(hasard(0,7));
    while (couleurEgale(combi.p3,combi.p1) || couleurEgale(combi.p3,combi.p2)){
      combi.p3.couleur=attributionCouleur(hasard(0,7));
    }

    combi.p4.couleur=attributionCouleur(hasard(0,7));
    while (couleurEgale(combi.p4,combi.p1) || couleurEgale(combi.p4,combi.p2) || couleurEgale(combi.p4,combi.p3)){
      combi.p4.couleur=attributionCouleur(hasard(0,7));
    }
    return combi;
  }

  // 2)a) Proposition utilisateur

  static class Coup {
    // type agrégé permettant de modeliser un coup : il contient la combinaison secrète et celle saisie,
    // ainsi que la réponse de leur comparaison sous la forme du nombre de symbole ('#' ou '*') à afficher
    Combinaison secrete=new Combinaison();
    Combinaison saisie=new Combinaison();
    int nbBienPlace;
    int nbMalPlace;
  }

  // entrée : caractere c (caractère couleur)
  static boolean verifCouleur(char c){
    // est retourné : vrai si la lettre contenue dans c correspond à une couleur valide, faux sinon
    return (c=='R' || c=='B' || c=='J' || c=='V' || c=='O' || c=='G' || c=='I' || c=='F');
  }

  static Combinaison saisieCombinaison(){
    // procéde à une demande de saisie des 4 caractères couleur de la combinaison
    // puis vérifie la validité de la saisie
    // est retourné : la combinaison valide saisie
    Combinaison combi=new Combinaison();

    Ecran.afficherln("Saisissez votre combinaison (4 lettres en majuscules les unes après les autres) : ");
    combi.p1.couleur=Clavier.saisirChar();
    combi.p2.couleur=Clavier.saisirChar();
    combi.p3.couleur=Clavier.saisirChar();
    combi.p4.couleur=Clavier.saisirChar();

    while (!verifCouleur(combi.p1.couleur) || !verifCouleur(combi.p2.couleur) || !verifCouleur(combi.p3.couleur) || !verifCouleur(combi.p4.couleur)){
      Ecran.afficherln("Erreur de saisie, une ou plusieurs des couleurs que vous avez rentrées n'existent pas ! Les couleurs autorisées sont : R, B, J, V, O, G, I, et F.");
      Ecran.afficherln("Veuillez recommencer la saisie : ");
      combi.p1.couleur=Clavier.saisirChar();
      combi.p2.couleur=Clavier.saisirChar();
      combi.p3.couleur=Clavier.saisirChar();
      combi.p4.couleur=Clavier.saisirChar();
    }
    return combi;
  }

  // 2)b) Réponse saisie utilisateur

  // entrée : Coup cp (le coup à analyser)
  static void nbSymboleBienPlace(Coup cp){
    // le coup placé en paramètre voit l'information contenue dans sa variable nbBienPlace mise à jour
    // si le pion effectivement bien placé, on l'indique par la mise à jour du boolen bienPlace contenu dans le pion en question
    int nbSymbole=0;
    if (couleurEgale(cp.saisie.p1,cp.secrete.p1)) {
      cp.saisie.p1.bienPlace=true;
      nbSymbole++;
    }
    if (couleurEgale(cp.saisie.p2,cp.secrete.p2)) {
      cp.saisie.p2.bienPlace=true;
      nbSymbole++;
    }
    if (couleurEgale(cp.saisie.p3,cp.secrete.p3)) {
      cp.saisie.p3.bienPlace=true;
      nbSymbole++;
    }
    if (couleurEgale(cp.saisie.p4,cp.secrete.p4)) {
      cp.saisie.p4.bienPlace=true;
      nbSymbole++;
    }

    cp.nbBienPlace=nbSymbole;
  }

  // entrée : Coup cp (le coup à analyser)
  static void nbSymboleMalPlace(Coup cp){
    // le coup placé en paramètre voit l'information contenue dans sa variable nbMalPlace mise à jour
    // on prend ici en compte que si le pion ou un de ses doublons éventuels est déjà bien placé, c'est le '#' qui l'emporte (if (!cp.saisie.p1.bienPlace){} et !cp.saisie.p2.bienPlace && couleurEgale() )
    // bienPlace est mis-à-jour dans la fonction ci-dessus
    // si le pion effectivement mal placé, on l'indique par l'incrémentation du nombre de symbole '*' à afficher
    int nbSymbole=0;
    if (!cp.saisie.p1.bienPlace){
      if ((!cp.saisie.p2.bienPlace && couleurEgale(cp.saisie.p1,cp.secrete.p2)) || (!cp.saisie.p3.bienPlace && couleurEgale(cp.saisie.p1,cp.secrete.p3)) || (!cp.saisie.p4.bienPlace && couleurEgale(cp.saisie.p1,cp.secrete.p4))){
        nbSymbole++;
      }
    }

    cp.saisie.p2.doublon=couleurEgale(cp.saisie.p2,cp.saisie.p1);
    if (!cp.saisie.p2.bienPlace && !cp.saisie.p2.doublon){
      if ((!cp.saisie.p1.bienPlace && couleurEgale(cp.saisie.p2,cp.secrete.p1)) || (!cp.saisie.p3.bienPlace && couleurEgale(cp.saisie.p2,cp.secrete.p3)) || (!cp.saisie.p4.bienPlace && couleurEgale(cp.saisie.p2,cp.secrete.p4))){
        nbSymbole++;
      }
    }

    cp.saisie.p3.doublon=(couleurEgale(cp.saisie.p3,cp.saisie.p1) || couleurEgale(cp.saisie.p3,cp.saisie.p2));
    if (!cp.saisie.p3.bienPlace && !cp.saisie.p3.doublon){
      if ((!cp.saisie.p1.bienPlace && couleurEgale(cp.saisie.p3,cp.secrete.p1)) || (!cp.saisie.p2.bienPlace && couleurEgale(cp.saisie.p3,cp.secrete.p2)) || (!cp.saisie.p4.bienPlace && couleurEgale(cp.saisie.p3,cp.secrete.p4))){
        nbSymbole++;
      }
    }

    cp.saisie.p4.doublon=(couleurEgale(cp.saisie.p4,cp.saisie.p1) || couleurEgale(cp.saisie.p4,cp.saisie.p2) || couleurEgale(cp.saisie.p4,cp.saisie.p3));
    if (!cp.saisie.p4.bienPlace && !cp.saisie.p4.doublon){
      if ((!cp.saisie.p1.bienPlace && couleurEgale(cp.saisie.p4,cp.secrete.p1)) || (!cp.saisie.p2.bienPlace && couleurEgale(cp.saisie.p4,cp.secrete.p2)) || (!cp.saisie.p3.bienPlace && couleurEgale(cp.saisie.p4,cp.secrete.p3))){
        nbSymbole++;
      }
    }
    cp.nbMalPlace=nbSymbole;
  }

  // entrée : Coup cp (le coup analysé)
  // cp valide
  static void affichageReponse(Coup cp){
    // est affiché : la réponse à la proposition saisie par l'utilisateur
    nbSymboleBienPlace(cp);
    nbSymboleMalPlace(cp); // remarque : l'ordre ici est important car il permet de mettre à jour le booléen bienPlace des pions en premier ('#' prioritaire), avant de calculer le nombre de '*'

    Ecran.afficherln("Réponse pour la combinaison saisie :");
    Ecran.afficher(afficherCombi(cp.saisie)," ");

    // affichage des pions bien placés
    for (int i=1;i<=cp.nbBienPlace;i++){
      Ecran.afficher('#');
    }

    // affichage des pions mal placés
    for (int i=1;i<=cp.nbMalPlace;i++){
      Ecran.afficher('*');
    }
  }

  // 3) La partie

  static class Partie {
    // type agregé d'une partie, composé du coup en cours, du nombre de tours effectués, et d'un booléen déterminant si l'utilisateur a gagné ou non
    Coup cp=new Coup();
    int tour;
    boolean gagne;
  }

  // entrée : entier coups_max (le nombre d'essai maximum pour que l'utilisateur trouve la combinaison)
  // coups_max > 0
  static void afficherRegles(int coups_max){
    // sont affichées : les règles du jeu
    Ecran.afficherln("Bienvenue dans le jeu du Mastermind. Voici les règles du jeu :\n");
    Ecran.afficherln("Une combinaison de 4 couleurs différentes va être tirée au hasard parmis ces 8 couleurs : ");
    Ecran.afficherln("Rouge (R), Bleu (B), Jaune (J), Vert (V), Orange (O), Gris (G), Indigo (I), et Fuschia (F)\n");
    Ecran.afficherln("Le but du jeu est de découvrir cette combinaison en moins  de ",coups_max," coups.");
    Ecran.afficherln("Pour cela, vous devrez trouver les quatre bonnes couleurs dans le bon ordre.\n");
    Ecran.afficherln("La réponse à votre proposition va être donnée sous la forme '#**' ");
    Ecran.afficherln("Chaque '#' représente une couleur bien placée, et chaque '*' une couleur mal placée.");
    Ecran.afficherln("En l'absence totale de symbole, il n'y a donc aucune couleur correspondante.\n");
    Ecran.afficherln("Puisse le sort vous être favorable !\n");

  }

  static Partie initJeu(int coups_max){
    // initialisation du premier coup
    // les règles du jeu sont affichées
    // est retourné : la partie pour le commencement du jeu, dont la combinaison secrète a été tirée au hasard et dont les variables tour et gagne ont été initialisées

    Partie p=new Partie();

    p.cp.secrete=combiHasard();
    p.tour=0;
    p.gagne=false;

    afficherRegles(coups_max);
    Ecran.afficherln("\nLa partie commence...\n\n");

    return p;
  }

  // entrée entier coup_max (nombre d'essai maximum de l'utilisateur)
  // entrée Partie p (la partie initialisée précédemment)
  // coup_max > 0
  static void jeu(int coup_max,Partie p){
    // exécute le déroulement de la partie, jusqu'à ce que le nombre de tour soit épuisé, ou que l'utilisateur gagne
    // après avoir appelé cette action, la partie contenue dans l'action principale sera telle qu'elle est au dernier tour joué

    // remarque : le nombre de tour est initialisé à 0 et est incrémenté au début de chaque boucle,
    // car sinon, lors d'une saisie gagnante, ce compteur serait alors incrémenté une fois de trop en fin de boucle

    do {
      p.tour++;
      Ecran.afficherln("\nTOUR ",p.tour);
      p.cp.saisie=saisieCombinaison();
      affichageReponse(p.cp);
      p.gagne=(p.cp.nbBienPlace==4); // vérification qui détermine si le coup est gagnant ou non
      Ecran.afficher("\n");
    } while (p.tour<=coup_max-1 && !p.gagne);
  }


  // entrée Partie p (la partie sortant du déroulement du jeu ci-dessus)
  static void finJeu(Partie p){
    // est affiché : la conclusion de la partie, selon le booléen gagne de la partie
    if (p.gagne){
      Ecran.afficherln("\nBRAVO, combinaison trouvée en ",p.tour," coup(s) !");
    } else {
      Ecran.afficherln("\nPERDU. La combinaison secrète était : ", afficherCombi(p.cp.secrete));
    }
    Ecran.afficherln("\n==== FIN DE LA PARTIE ====");
  }

  // action principale
  public static void main(String[] args){
    // test de validité des parties 1) et 2) :

    // Combinaison combiH=new Combinaison();
    // Coup cp=new Coup();

    // combiH=combiHasard();
    // combiH=Ecran.afficherln(afficherCombi(combiH));

    // cp.secrete=combiH;
    // cp.saisie=saisieCombinaison();
    // affichageReponse(cp);


    // action principale de la partie 3)
    // données
    final int NB_COUPS=10; // constante contenant le nombre d'essai maximum pour le joueur
    Partie p=new Partie();

    // initialisation et déroulement du jeu
    p=initJeu(NB_COUPS);
    Ecran.afficherln("Combinaison secrète, pour effectuer les tests :\n",afficherCombi(p.cp.secrete));
    jeu(NB_COUPS,p);

    // résultats & conclusion
    finJeu(p);
  }

}
